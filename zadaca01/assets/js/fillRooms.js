const roomItems = [
    {
        name: "Predavaonica 1",
        itemsCount: "50"
    },
    
    {
        name: "Predavaonica 2",
        itemsCount: "50"
    },
    
    {
        name: "Predavaonica 3",
        itemsCount: "50"
    },
    
    {
        name: "Predavaonica 4",
        itemsCount: "50"
    },
    
    {
        name: "Predavaonica 5",
        itemsCount: "50"
    },
    
    {
        name: "Predavaonica 6",
        itemsCount: "50"
    },
    
    {
        name: "Portirnica",
        itemsCount: "50"
    },
    
    {
        name: "Referada",
        itemsCount: "50"
    }
]

function fillRooms() {

    for(let i=0; i<roomItems.length; i++) {
        const li = document.createElement('li');
        const h1 = document.createElement('h1');
        const p = document.createElement('p');

        h1.className = "";
        h1.textContent = roomItems[i].name;
        p.className = "under";
        p.textContent = "Broj stvari: " + roomItems[i].itemsCount;
        li.appendChild(h1);
        li.appendChild(p);
        document.getElementById('roomsList').appendChild(li);
    }
}


function filterRooms() {
    var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
    list = document.getElementById("roomsList");

    switching = true;
    dir = "asc";


    while(switching) {
        switching = false;
        b = list.getElementsByTagName("li");
        for(i = 0; i<(b.length-1); i++){
            shouldSwitch = false;
            if(dir == "asc") {
                if(b[i].innerHTML.toLocaleLowerCase() > b[i+1].innerHTML.toLocaleLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if(b[i].innerHTML.toLocaleLowerCase() < b[i+1].innerHTML.toLocaleLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if(shouldSwitch) {
            b[i].parentNode.insertBefore(b[i+1], b[i]);
            switching = true;

            switchcount++;
            
        } else {
            if(switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }

    if(dir == 'asc') {
        let arrow = document.getElementsByClassName('arrow')[0];
        arrow.style.transform = "rotate(0deg)";
    } else {
        let arrow = document.getElementsByClassName('arrow')[0];
        arrow.style.transform = "rotate(180deg)";
    }
}