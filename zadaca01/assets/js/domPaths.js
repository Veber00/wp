path = [];

let node1 = "";
let node2 = "";

function min_path(event) {

    if(node1 == "") {
        node1 = event.target;
    } else {

        node2 = event.target;

        if(node1 === node2) {
            return node1;
        }

        var node_1_ancestors = get_ancestors(node1);
        var node_2_ancestors = get_ancestors(node2);

        var divergent_index = 0;
        while(node_1_ancestors[divergent_index] === node_2_ancestors[divergent_index]) {
            divergent_index++;
        }

        path = [];
        for(var i = node_1_ancestors.length - 1; i >= divergent_index - 1; i--) {
            path.push(node_1_ancestors[i]);
        }
        for(var i = divergent_index; i < node_2_ancestors.length; i++) {
            path.push(node_2_ancestors[i]);
        }
        

        print();

        node1 = "";
        node2 = "";
    }
}

function get_ancestors(node) {
    var ancestors = [node];
    while(ancestors[0] !== null) {
        ancestors.unshift(ancestors[0].parentElement);
    }
    return ancestors;
}

function print () {
    for(var i=0; i<path.length; i++) {
        console.log(path[i].nodeName);
    }
}