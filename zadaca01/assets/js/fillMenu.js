const menuItems = [
    {
        name: "Inventura",
        image: {
            src: "./assets/img/News.svg",
            alt: ""
        },
        link: "#index"
    },
    
    {
        name: "Inventar",
        image: {
            src: "./assets/img/Box.svg",
            alt: ""
        },
        link: "#Inventar"
    },
    
    {
        name: "Prostorije",
        image: {
            src: "./assets/img/Classroom.svg",
            alt: ""
        },
        link: "#Prostorije"
    },
    
    {
        name: "Zaposlenici",
        image: {
            src: "./assets/img/Contacts.svg",
            alt: ""
        },
        link: "#Zaposlenici"
    },
    
    {
        name: "Administracija",
        image: {
            src: "./assets/img/Services.svg",
            alt: ""
        },
        link: "#Administracija"
    }
]

function fillMenu() {

    for(i=0; i<menuItems.length; i++) {
        const li = document.createElement('li');
        const img = document.createElement('img');
        const a = document.createElement('a');

        a.text = menuItems[i].name;
        a.href = menuItems[i].link;
        img.src = menuItems[i].image.src;
        li.appendChild(img);
        li.appendChild(a);
        document.getElementById('menuList').appendChild(li);
    }
}