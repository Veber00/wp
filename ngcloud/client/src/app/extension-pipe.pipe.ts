import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extensionPipe'
})
export class ExtensionPipePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    var ext = value.name.split(".", 2)[1];
    var ret = "file.png";

    if(ext === "jpg") {
      ret = "jpg.png"
    } 
    else if (ext === "mp3") {
      ret = "mp3.png"
    } 
    else if (ext === "pdf") {
      ret = "pdf.png"
    } 
    else if (ext === "ppt") {
      ret = "ppt.png"
    } 
    else if (ext === "pptx") {
      ret = "pptx.png"
    } 
    else if (ext === "txt") {
      ret = "txt.png"
    } 
    else if (ext === "doc") {
      ret = "doc.png"
    } 
    else if (ext === "docx") {
      ret = "docx.png"
    } 
    else if (ext === "xls") {
      ret = "xls.png"
    } 
    else if (ext === "xlsx") {
      ret = "xlsx.png"
    } 
    
    return ("assets/img/" + ret);
  }

}
