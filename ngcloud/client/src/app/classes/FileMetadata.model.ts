export class FileMetadata {
    name: string;
    added: string;
    size: number;
    id: string;
}