import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FilesComponent } from './components/files/files.component';
import { ListFilesComponent } from './components/files/list-files/list-files.component';
import { NewFileComponent } from './components/new-file/new-file.component';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'files'},
  { path: 'files', component: ListFilesComponent},
  { path: 'files/new', component: NewFileComponent},
  { path: 'files/profile', component: ProfileComponent},
  { path: '**', redirectTo: 'files'}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

 }
