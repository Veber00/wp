import { Component, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FileMetadata } from 'src/app/classes/FileMetadata.model';
import { FilesService } from '../files/files.service';
import { DatePipe, formatDate } from '@angular/common';
import { summaryFileName } from '@angular/compiler/src/aot/util';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-file',
  templateUrl: './new-file.component.html',
  styleUrls: ['./new-file.component.scss'],
  providers: [DatePipe]
})
export class NewFileComponent implements OnInit, OnDestroy {


  newFileForm: FormGroup = new FormGroup({
    name: new FormControl(null, [Validators.required])
  });

  private createFileSubscription: Subscription;

  constructor(
    public _route: Router,
    public filesService: FilesService
    ) { }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    this.createFileSubscription.unsubscribe();
  }

  onSubmit() {
    const { name } = this.newFileForm.controls;
    let options = { year: 'short'}
    const newFile: FileMetadata = {
      name: name.value,
      added: new Date().toLocaleString('en-US'),
      size: randomNum(1, 10),
      id: ""
    }
    this.createFileSubscription = this.filesService.postFile(newFile).subscribe(() => this._route.navigate(['/files']));
  }

  AllFilesNav(): void {
    this._route.navigate(['/files']);
  }

}

function randomNum(min, max) {
  return Math.floor((Math.random() * (max - min) + min).toString());
}
