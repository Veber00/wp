import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileMetadata } from 'src/app/classes/FileMetadata.model';
import { tap } from 'rxjs/operators';

@Injectable()
export class FilesService {

  files: FileMetadata[] = [
    { name: "todo.docx", added: "5/1/19, 8:38 PM", size: 0.01, id: "0"},     { name: "1.png", added: "5/1/19, 8:38 PM", size: 0.00, id: "1"},     { name: "02 Hey Brother.mp3", added: "5/1/19, 8:38 PM", size: 9.98, id: "2"},     { name: "us_2018.xlsx", added: "5/1/19, 8:38 PM", size: 0.02, id: "3"},     { name: "OM_predlozak.pptx", added: "5/1/19, 8:38 PM", size: 0.96, id: "4"} 
  ];

  private readonly url = '/api/files';

  public filesSubject: BehaviorSubject<FileMetadata[]> = new BehaviorSubject<FileMetadata[]>(null);

  constructor(public http: HttpClient) { 
    this.getFiles().subscribe();
  }

  getFiles(): Observable<FileMetadata[]> {
    return this.http.get<FileMetadata[]>(this.url)
      .pipe(
        tap((files: FileMetadata[]) => this.filesSubject.next(files))
      );
  }

  postFile(file: FileMetadata): Observable<FileMetadata> {
    return this.http.post<FileMetadata>(this.url, file)
      .pipe(
        tap((file: FileMetadata) => this.filesSubject.next([...this.filesSubject.getValue(), file]))
      );
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.url}/${id}`)
    .pipe(
      tap(() => this.filesSubject.next(this.filesSubject.getValue().filter((file: FileMetadata) => file.id !== id.toString())))
    );
  }

}
