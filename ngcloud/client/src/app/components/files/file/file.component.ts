import { stringify } from '@angular/compiler/src/util';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FileMetadata } from 'src/app/classes/FileMetadata.model';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {

  @Input() file: FileMetadata;

  @Output()
  deleteClick: EventEmitter<FileMetadata> = new EventEmitter<FileMetadata>();

  constructor() { 
  }

  ngOnInit(): void {
  }

  deleteFile() {
    this.deleteClick.emit(this.file);
  }

}
