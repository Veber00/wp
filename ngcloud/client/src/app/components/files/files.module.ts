import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilesComponent } from './files.component';
import { FileComponent } from './file/file.component';
import { ExtensionPipePipe } from 'src/app/extension-pipe.pipe';
import { ListFilesComponent } from './list-files/list-files.component';
import { NewFileComponent } from '../new-file/new-file.component';
import { ProfileComponent } from '../profile/profile.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilesService } from './files.service';




@NgModule({
  declarations: [
    FilesComponent,
    FileComponent,
    ExtensionPipePipe,
    ListFilesComponent,
    NewFileComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FilesComponent,
    FileComponent,
    ExtensionPipePipe,
    ListFilesComponent,
    NewFileComponent,
    ProfileComponent
  ],
  providers: [FilesService]
})
export class FilesModule { }
