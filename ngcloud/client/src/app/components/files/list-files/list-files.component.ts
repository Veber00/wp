import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { FileMetadata } from 'src/app/classes/FileMetadata.model';
import { FilesService } from '../files.service';

@Component({
  selector: 'app-list-files',
  templateUrl: './list-files.component.html',
  styleUrls: ['./list-files.component.scss']
})
export class ListFilesComponent implements OnInit, OnDestroy {

  files: FileMetadata[];

  private filesSubscription: Subscription;
  filesSubject: BehaviorSubject<FileMetadata[]>

  constructor(
    public _router: Router,
    public filesService: FilesService
    ) { }

  ngOnInit(): void {
    // this.filesSubscription = this.filesService.filesSubject.subscribe((files: FileMetadata[]) => {
    //   this.files = files;
    // });
    this.filesSubject = this.filesService.filesSubject;
  }

  NewFileNav(): void {
    this._router.navigate(['/files/new']);
  }

  ngOnDestroy(): void {
    
  }

  deleteFile(file: FileMetadata){
    this.filesService.delete(file.id).subscribe();
  }
}
