const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let files = [
    {   
        name: "todo.docx", 
        added: "5/1/19, 8:38 PM", 
        size: 0.01, 
        id: "0"
    },     
    {   
        name: "1.png", 
        added: "5/1/19, 8:38 PM", 
        size: 0.00, 
        id: "1"
    },     
    { 
        name: "02 Hey Brother.mp3", 
        added: "5/1/19, 8:38 PM", 
        size: 9.98, 
        id: "2"
    },     
    {
        name: "us_2018.xlsx", 
        added: "5/1/19, 8:38 PM", 
        size: 0.02, 
        id: "3"
    },     
    { 
        name: "OM_predlozak.pptx", 
        added: "5/1/19, 8:38 PM", 
        size: 0.96, 
        id: "4"
    }
 ];

let currentId = 5;

const route = '/api/files'

app.post(route, (req, res) => {
    const body = req.body;
    const file = {...body, id: (currentId++).toString()};
    files.push(file);
    res.status(201);
    res.send(file);
});

app.get(route, (req, res) => {
    res.send(files);
});

app.get(`${route}/:id`, (req, res) => {
    const id = Number(req.params.id);
    const file = files.find(file => file.id.toString() === id.toString());
    if(!file) {
        res.status(404);
        res.send({error: "Not found!"});
    }
    res.send(file);
});

app.delete(`${route}/:id`, (req, res) => {
    const id = Number(req.params.id);
    files = files.filter(file => file.id.toString() !== id.toString());
    res.send();
});

app.listen(3000, () => console.log('Listening on port 3000'));